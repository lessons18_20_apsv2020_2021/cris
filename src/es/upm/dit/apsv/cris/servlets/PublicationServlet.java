package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;

@WebServlet("/PublicationServlet")//give path to the server
public class PublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;//authomatic static atribute. IMPORTANT
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = (String) request.getParameter("id");
		Client client = ClientBuilder.newClient(new ClientConfig());
		Publication pi = client.target("http://localhost:8080/CRISSERVICE/rest/Publications/" 
		         + id)//convert json to researcher
		        .request().accept(MediaType.APPLICATION_JSON).get(Publication.class);
		request.setAttribute("pi",pi);
		
		getServletContext().getRequestDispatcher("/PublicationsView.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}


}
